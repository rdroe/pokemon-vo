import React from 'react'
import BaseUi from './BaseUI'
import { getList, saveList } from './utilities/data'
import AppContext from './context/App'
import { PokemonList } from './db'

const App: React.FC<{}> = () => {

    const [items, setItems] = React.useState(null)

    React.useEffect(() => {
        getList().then((pokeList) => {
            setItems(pokeList)
        })
    }, [])

    return (<div>
        <AppContext.Provider value={{
            items,
            setItems: (pl: PokemonList) => {
                setItems(pl)
                saveList(pl)
            }
        }}>
            <BaseUi />
        </AppContext.Provider>
    </div >)
}

export default App
