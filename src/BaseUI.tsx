import { Client as Styletron } from 'styletron-engine-atomic'
import { Provider as StyletronProvider } from 'styletron-react'
import { LightTheme, BaseProvider, styled } from 'baseui'
import React from 'react'

import { HeadingMedium } from 'baseui/typography'

const engine = new Styletron()
const Centered = styled('div', {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyItems: 'center',
    height: '100%',
})

const PokemonDropdown = React.lazy(() => import('./components/PokemonDropdown'))

const ChosenPokes = React.lazy(() => import('./components/ChosenPokes'))


const BaseUI: React.FC<{}> = () => {

    return <StyletronProvider value={engine}>
        <BaseProvider theme={LightTheme}>
            <Centered>
                <HeadingMedium >pokemon Vo</HeadingMedium>
            </Centered>
            <Centered>
                <React.Suspense fallback={<div />}>
                    <PokemonDropdown />
                </React.Suspense>
                <React.Suspense fallback={<div />}>
                    <ChosenPokes />
                </React.Suspense>
            </Centered>
        </BaseProvider>
    </StyletronProvider >
}

export default BaseUI
