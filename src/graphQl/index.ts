import { gql, GraphQLClient } from 'graphql-request'
import { Pokemon } from '../db'

const endpoint = 'https://beta.pokeapi.co/graphql/v1beta'

const graphQLClient = new GraphQLClient(endpoint, { mode: 'cors' })

const query = gql`
{
  gen3_species: pokemon_v2_pokemonspecies(where: {pokemon_v2_generation: {name: {_eq: "generation-iii"}}, pokemon_habitat_id: {}}, order_by: {id: asc}) {
    name
    id
    pokemon_v2_pokemoncolor{
      name
    }
		capture_rate
    is_baby
    has_gender_differences
  }
}
`
export const fetchPokes = (cancel: boolean): Promise<{
    gen3_species: Pokemon[]
}> | null => {

    if (cancel) return null

    return graphQLClient.request(query)
}

