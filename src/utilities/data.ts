import { arrayMove } from 'baseui/dnd-list'
import db, { LIST, Pokemon, PokemonList } from '../db'

const ID_EMPTY = ''
export const NAME_EMPTY = '--'

const emptyPoke = (): Pokemon => ({
    id: ID_EMPTY,
    name: NAME_EMPTY
})

interface DataCtxGetter<T> {
    (): Promise<T>
}

interface DataCtxSetter<T> {
    (pokes?: PokemonList): Promise<T>
}

const isValidPoke = (poke: any): poke is Pokemon => {
    if (typeof poke !== 'object' || poke === null) {
        return false
    }
    return Object.keys(emptyPoke())
        .find((propName) => poke[propName] === undefined) === undefined
}

export const saveList: DataCtxSetter<number> = (pokeList: PokemonList) => db.chosenPokes.put(pokeList)

export const mapNames = (pokeList: { list } | null): string[] => {
    if (pokeList === null) return []
    return (pokeList.list ? pokeList.list : []).map(({ name }) => name)
}
export const getList: DataCtxGetter<PokemonList> = async () => {

    const list = await db.chosenPokes.get(LIST)
    if (list === undefined) {
        await initList()
        return db.chosenPokes.get(LIST)
    }
    return list
}

export const initList: DataCtxSetter<number> = () => saveList({
    id: LIST,
    list: [
        emptyPoke(),
        emptyPoke(),
        emptyPoke(),
        emptyPoke(),
        emptyPoke(),
        emptyPoke(),
    ]
})

const getEmptySlot = async (chosenPokes: PokemonList): Promise<number | null> => {
    let ret: number | null = null;

    chosenPokes.list.forEach(
        (pokeOrSlot: Pokemon, idx: number) => {
            if (ret === null) {
                if (pokeOrSlot.id === ID_EMPTY) {
                    ret = idx
                }
            }
        })
    return ret
}

export const choosePoke = async (chosenPoke: Pokemon, chosenPokes: PokemonList, setItems: (list: PokemonList) => void) => {

    if (!isValidPoke(chosenPoke)) return

    const firstEmpty = await getEmptySlot(chosenPokes)

    if (firstEmpty !== null) {
        chosenPokes.list[firstEmpty] = chosenPoke
        setItems({ ...chosenPokes })
    }
}

const removePoke = (chosenPokes: PokemonList, oldIndex: number, setItems: (list: PokemonList) => void) => {

    const cp: PokemonList = {
        ...chosenPokes,
        list: [
            ...chosenPokes.list
        ]
    }
    cp.list[oldIndex] = emptyPoke()
    setItems(cp)

}

export const movePoke = (
    { oldIndex, newIndex }: { oldIndex: number, newIndex: number },
    chosenPokes: PokemonList,
    setItems: (list: PokemonList) => void) => {

    if (newIndex === -1) return removePoke(chosenPokes, oldIndex, setItems)

    chosenPokes.list = arrayMove(chosenPokes.list, oldIndex, newIndex)
    setItems({ ...chosenPokes })
}


export const defaultImage = (id: string) => {
    return `url(https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png)`
}
