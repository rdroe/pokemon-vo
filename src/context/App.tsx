import { createContext, Dispatch } from 'react'
import { PokemonList } from '../db'

export default createContext<{
    items: null | PokemonList,
    setItems: Dispatch<any>
}>({
    items: null,
    setItems: () => { }
})
