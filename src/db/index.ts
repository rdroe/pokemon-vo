import Dexie from 'dexie'


export const LIST = 0

export interface Pokemon {
    id: string,
    name: string
    pokemon_v2_pokemoncolor?: {
        name: string
    },
    capture_rate?: number,
    is_baby?: boolean,
    has_gender_differences?: boolean
}

export interface PokemonList {
    id?: number
    list: Pokemon[]
}

interface Cache {
    id?: number
    url: string
    fetched: any
}

export class PokeDb extends Dexie {
    chosenPokes: Dexie.Table<PokemonList, number>
    cache: Dexie.Table<Cache, number>

    constructor() {
        super('PokeDb')
        this.version(1.1).stores({
            chosenPokes: 'id',
            cache: 'id++'
        })
        this.chosenPokes = this.table('chosenPokes')
        this.cache = this.table('cache')
    }
}

export default new PokeDb

