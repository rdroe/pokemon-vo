import * as React from 'react'
import { useStyletron } from 'baseui'
import { StatefulPopover, PLACEMENT, TRIGGER_TYPE } from 'baseui/popover'
import { ParagraphSmall } from 'baseui/typography'
import { PokemonList } from '../db'
import { NAME_EMPTY } from '../utilities/data'
interface PopProps {
    pokeLookup: PokemonList
    children: React.ReactChildren
    pokeName: string
}

const lookupPokeData = (data: PokemonList, name: string, keys: string[]) => {
    if (!data.list) return
    if (typeof name === 'string') {

        const poke = data.list.find(({ name: pId }) => name === pId)
        let ret: any = poke
        keys.forEach((k) => {

            ret = ret[k] === undefined ? ret : ret[k]
        })
        return typeof ret === 'boolean' ? ret.toString() : ret
    } else return ''

}

const isEmptyPoke = (pokeName: string) => {
    if (pokeName === NAME_EMPTY) return true
}

const PopoverText: React.FC<{ pokeName: string, isGendered: boolean, colorName: string, close?: Function }> = ({ pokeName, isGendered, colorName }) =>
    <ParagraphSmall
        backgroundColor="black"
        color="white"
        maxWidth="200px"
        overrides={{
            Block: {
                style: {
                    padding: '1vw',
                    marginTop: '0',
                    marginBottom: '0',
                    borderRadius: '4%'
                }
            }
        }}
    >
        name: {pokeName}
        <br />
    color: {colorName}
        <br />
            is gendered?: {isGendered}
    </ParagraphSmall>


export default function({ children, pokeLookup, pokeName }: PopProps): JSX.Element {

    if (isEmptyPoke(pokeName)) return (<div style={{ visibility: 'hidden' }}>{children}</div>)

    const colorName = lookupPokeData(pokeLookup, pokeName, ['pokemon_v2_pokemoncolor', 'name'])

    const isGendered = lookupPokeData(pokeLookup, pokeName, ['has_gender_differences'])

    const [css] = useStyletron()



    return (
        <div className={css({ overflow: 'auto', display: 'flex' })}>
            <div
                className={css({
                    height: '100%',
                    width: '100%',
                    overflow: 'auto'
                })}
            >
                <StatefulPopover
                    initialState={{ isOpen: false }}
                    dismissOnEsc={false}
                    ignoreBoundary={true}
                    onMouseEnterDelay={500}
                    placement={PLACEMENT.left}

                    showArrow
                    content={({ close }) => <PopoverText pokeName={pokeName} isGendered={isGendered} colorName={colorName} close={close} />}
                    overrides={{
                        Body: {
                            style: {
                                backgroundColor: 'black'
                            }
                        },
                        Arrow: {
                            style: ({ $theme }) => ({
                                backgroundColor: 'black',
                            }),
                        },
                    }}
                >
                    {children}
                </StatefulPopover>
            </div>
        </div >
    )
}
