import * as React from 'react'
import { List } from 'baseui/dnd-list'
import { mapNames, movePoke } from '../utilities/data'
import App from '../context/App'
import ChosenPokesItem from './ChosenPokesItem'

const ChosenPokes: React.FC<{}> = () => {
    const { items, setItems } = React.useContext(App)
    return (
        <List
            removable
            removableByMove
            onChange={(change) => movePoke(change, items, setItems)}
            items={mapNames(items || { list: [] })}
            overrides={{
                List: {
                    style: {
                        maxWidth: '500px',
                        marginLeft: 'auto',
                        marginRight: 'auto'
                    }
                },
                Label: {
                    style: () => ({
                        width: '100%'
                    }),
                    component: (props) => <ChosenPokesItem $value={props.$value} items={items} />
                }
            }}
        />
    )
}

export default ChosenPokes
