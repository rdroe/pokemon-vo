import * as React from 'react'
import { useStyletron } from "baseui";
import { defaultImage } from '../utilities/data';


const PokeImage: React.FC<{ label: string | number }> = ({ label: id }) => {

    const [css] = useStyletron()
    return (
        <div
            className={css({
                width: '40px',
                height: '40px',
                backgroundImage: defaultImage(id.toString()),
                backgroundSize: 'cover',
                float: 'left'
            })}
        >

        </div>

    );
}

export default PokeImage
