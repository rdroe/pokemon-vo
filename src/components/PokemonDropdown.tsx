import React from 'react'
import { useStyletron } from 'baseui'
import { Select } from 'baseui/select'
import { FormControl } from 'baseui/form-control'
import { choosePoke, mapNames, NAME_EMPTY } from '../utilities/data';
import { fetchPokes } from '../graphQl';
import App from '../context/App';
import { Pokemon, PokemonList } from '../db';
import PokeWithImage from './Option';

const { useState, useEffect, useMemo, useContext } = React

// utils
const chosenNamesHash = (chosenPokes: PokemonList) =>
    mapNames(chosenPokes).join('-')

function mapper(option: Pokemon): string {
    return option.name
}

const PokemonDropdown: React.FC<{}> = () => {

    const [css] = useStyletron()
    const [value] = useState('')
    const [isDisabled, setIsDisabled] = useState(false)
    const [rawFetch, setRawFetch] = useState(null)
    const { items: chosenPokes, setItems } = useContext(App)

    useEffect(() => {
        fetchPokes(false).then((data) => setRawFetch(data))
        // run only once
    }, [])

    useEffect(() => {
        setIsDisabled(
            chosenPokes?.list?.filter(({ id }) => id !== '').length > 5 ? true : false
        )
        // re-run when a name changes
        // (could probably change to an array-length check)
    }, [chosenNamesHash(chosenPokes)])


    const filteredOptions = useMemo(() => {
        if (!rawFetch || !rawFetch?.gen3_species) return
        return rawFetch.gen3_species.reduce((accum, option: Pokemon) => {

            const optionAsString = mapper(option)

            // exclude already chosen pokes 
            if (mapNames(chosenPokes).includes(optionAsString)) return accum

            // for possible return, add "label" prop (poke name)
            const labeled = {
                ...option,
                label: option.name
            }

            // allow if contains user-entered values, or if no value is entered
            if (optionAsString.includes(value) || !value) {
                return accum.concat([labeled])
            }

            // reducer adds nothing 
            return accum
        }, [])

        // re-run options filter  when
        // - typed text changes,
        // - a fetch brings back data, or
        // - a new poke is chosen
    }, [value, rawFetch, chosenNamesHash(chosenPokes)])


    return (
        <div className={css({
            minWidth: '265px'
        })}>
            <FormControl
                overrides={{
                    Label: {
                        style: {
                            display: 'block',
                            textAlign: 'center'
                        }
                    }
                }}
                label="Track Your 6 Favorite Pokemon Species!">
                <Select
                    disabled={isDisabled}
                    onChange={(params) => {
                        const option = params.option
                        choosePoke({
                            ...option,
                            id: option?.id || '',
                            name: option.label?.toString() || NAME_EMPTY,
                        }, chosenPokes, setItems)
                    }}
                    placeholder="Choose a Pokemon"
                    maxDropdownHeight="245px"
                    options={filteredOptions}

                    getOptionLabel={({ option }) => <PokeWithImage label={option.id} name={option.label} />}
                />
            </FormControl>
        </div>
    );
}

export default PokemonDropdown
