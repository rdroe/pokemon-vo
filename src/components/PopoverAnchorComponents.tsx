import * as React from 'react'
import { PokemonList } from '../db'
import { Button, KIND, SHAPE, SIZE } from 'baseui/button'
import { Show } from 'baseui/icon'

export const Info = <Button
    kind={KIND.tertiary}
    shape={SHAPE.pill}
    size={SIZE.mini}
>
    <Show size={16} color="black" />
</ Button>


const PokePopover = React.lazy(() => import('./PokePopover'))

export const Popover: React.FC<{ items: PokemonList, value: React.ReactNode }> = ({ items, children, value }) =>
    <React.Suspense fallback={Info} >
        <div style={{ backgroundColor: 'transparent', marginLeft: 'auto', marginRight: '15%' }}>
            <PokePopover pokeName={value.toString()} pokeLookup={items}>
                {children}
            </PokePopover>
        </div>
    </React.Suspense>


