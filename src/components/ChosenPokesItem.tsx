import * as React from 'react'
import PokeImage from './PokeImage'
import { LabelLarge } from 'baseui/typography'
import { useStyletron } from 'baseui'
import { Popover, Info } from './PopoverAnchorComponents'
import { PokemonList } from '../db'

// List Item in the draggable list.
const ChosenPokesItem: React.FC<{ $value: string, items: PokemonList }> = ({ $value, items }) => {
    const [css] = useStyletron()
    return <div className={
        css({
            width: '100%',
        })
    }>
        <LabelLarge
            className={
                css({
                    display: 'flex',
                    paddingLeft: '10%',
                    alignItems: 'center'
                })
            }
        >
            <PokeImage label={items?.list.find(({ name }) => name === $value)?.id} />
            <div
                className={css({
                    position: 'relative',
                    left: '10%'
                })}
            >{$value}</div>
            {/* The Popover (which includes an async wrapper)*/}
            <Popover
                value={$value}
                items={items}
            >
                {/* Baseweb Popovers wrap the elements that anchor them, here an icon, at time of writing */}
                {Info}
            </Popover >
        </LabelLarge >
    </div >
}

export default ChosenPokesItem
