import * as React from 'react'
import { useStyletron } from "baseui"
import PokeImage from './PokeImage'


const PokeWithImage: React.FC<{ label: number, name: string }> = ({ label: id, name }) => {
    const [css] = useStyletron()
    return (
        <div
            className={css({
                display: 'flex',
                justifyContent: 'space-around',
                alignItems: 'center',
            })}
        >
            <PokeImage label={id} />
            <span
                className={css({
                    textAlign: 'center',
                })}>{name}</span>
        </div >
    );
}

export default PokeWithImage
