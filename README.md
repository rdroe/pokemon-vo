## Pokemon-VO
#### (improved from original submission)
See the original submission here: https://gitlab.com/rdroe/pokemon-vo/-/tree/original-submission


### To run the code

1 - Have node and yarn installed on your system. Acceptable node versions definitely include
  - The most recent version available beginning with '14'
  - The most recent version available beginning with '16'

2 - Clone the project; on the cli, enter: `git clone git@gitlab.com:rdroe/pokemon-vo.git`

3 - `cd` into the directory you just created, e.g. `cd pokemon-vo`

4 - Run `yarn start`. (This should not only install dependencies and build the app but also start a server.)

5 - When the build has completed, visit http://localhost:8080

#### Features
- Pokemon Sprite Display
- Pokemon species search-and-select combobox
- Real Poke names and data from [https://beta.pokeapi.co/graphql/console/](https://beta.pokeapi.co/graphql/console/)
- "x" on selected Pokes; click or tap for removal
- Drag-and-drop reordering of chosen pokes
- More-info Popover: click the Eye icon near a selected Poke's name
- Persistent data stored in the browser between visits
- Accessibility features

Some of the technology I used includes
- react
  - context
  - suspense api
- parcel build tool
  - code splitting (proof of concept toward smaller bundle size)
- baseweb from Uber for components
- dexie.js (a wrapper around IndexedDB for client-side persistence)

### Recent Fixes
- Improved Popover discoverability, usability, and readability
- No more lag on dropping an item
- Strategy introduced for reducing bundle size
- Easier-to-read list

